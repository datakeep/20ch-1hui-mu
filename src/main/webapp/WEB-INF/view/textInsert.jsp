<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<meta charset="utf-8">
<title>Welcome</title>
</head>
<body>
	<!-- 投稿をデータベースに追加 -->
    <h1>${sentence}</h1>
    <h2>${sentence1}</h2>
    <form:form modelAttribute="textForm">
    <!-- setter,getterのmessage -->
        <form:textarea path="message" cols="100" rows="5"/>
        <input type="submit" value="投稿しちゃうヨ">
    </form:form>
</body>
</html>