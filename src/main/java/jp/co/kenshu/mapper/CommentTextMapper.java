package jp.co.kenshu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.kenshu.entity.CommentText;

public interface CommentTextMapper {
	//全件取得
	List<CommentText> getCommentTextAll();

	//データベースにコメント追加
	//SpringのMapperでは引数二個入れる時は自動で色々されてしまうので@Paramつける決まり
	int insertCommentText(@Param("comment")String comment, @Param("message_id")int messageId);
}
