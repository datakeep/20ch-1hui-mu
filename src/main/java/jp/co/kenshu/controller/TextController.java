package jp.co.kenshu.controller;

import java.util.List;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.text.CommentTextDto;
import jp.co.kenshu.dto.text.TextDto;
import jp.co.kenshu.form.CommentTextForm;
import jp.co.kenshu.form.TextForm;
import jp.co.kenshu.service.TextService;

@Controller
public class TextController {

	@Autowired
    private TextService textService;

	//データベースに既にある投稿を一件取得
	//http://localhost:8080/20ch/20ch/1でホーム

	//value = "/20ch/"はconfigと連動
    @RequestMapping(value = "/20ch/{id}", method = RequestMethod.GET)
    public String text(Model model, @PathVariable int id) {
        TextDto text = textService.getText(id);
        //このsentenceはtext.jspに対応
        model.addAttribute("sentence", "投稿してちょ");
        model.addAttribute("text", text);
        return "text";//text.jspに入っていく
    }

	//データベースの投稿を全件取得
    //http://localhost:8080/20ch/20ch/

    //value = "/20ch/"はconfigと連動
    @RequestMapping(value = "/20ch/", method = RequestMethod.GET)
    public String textAll(Model model) {
        List<TextDto> texts = textService.getTextAll();
        //このsentenceはtextAll.jspに対応
        model.addAttribute("sentence", "投稿してちょ");
        model.addAttribute("texts", texts);

        //データベースのコメントを全件取得して表示
        List<CommentTextDto> commentTexts = textService.getCommentTextAll();
        //このsentenceはtextAll.jspに対応
        model.addAttribute("comments", commentTexts);

        //コメント入力フォームを表示(投稿全件取得の画面に表示させるからここ)
        CommentTextForm commentForm = new CommentTextForm();
        model.addAttribute("commentTextForm", commentForm);

        return "textAll";
    }

    //投稿をデータべースに追加
    //http://localhost:8080/20ch/20ch/insert/input/

    @RequestMapping(value = "/20ch/insert/input/", method = RequestMethod.GET)
    public String textInsert(Model model) {
        TextForm form = new TextForm();
        model.addAttribute("textForm", form);
        model.addAttribute("sentence", "投稿を入力してちょ");
        model.addAttribute("sentence1", "どんどん投稿してちょ");
        return "textInsert";
    }

    @RequestMapping(value = "/20ch/insert/input/", method = RequestMethod.POST)
    public String textInsert(@ModelAttribute TextForm form, Model model) {
        int count = textService.insertText(form.getMessage());
        Logger.getLogger(TextController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
        return "redirect:/20ch/";//Postされたら20chに遷移されますよ
    }

    //コメントをデータベースに追加
    //(コメントの全件取得表示は、投稿の全件取得表示画面で行われるのでここには書かない)

    @RequestMapping(value = "/20ch/", method = RequestMethod.POST)
    public String commentTextInsert(@ModelAttribute CommentTextForm commentForm, Model model) {
        int count1 = textService.insertCommentText(commentForm.getComment(), commentForm.getMessageId());
        Logger.getLogger(TextController.class).log(Level.INFO, "挿入件数は" + count1 + "件です。");
        return "redirect:/20ch/";//Postされたら20chに遷移されますよ
    }
}
