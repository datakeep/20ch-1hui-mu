<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta charset="utf-8">
        <title>Welcome</title>
    </head>
    <body>
     	<!-- データベースの投稿全件表示 -->
    	<!-- このsentence,texts,textはControllerと対応 -->

        <h1>${sentence}</h1>

        <!-- var=textはドルカッコ内のtextと一致 -->
        <c:forEach items="${texts}" var="text">
        	<!-- getter.setterのmessageと対応 -->
            <p><c:out value="${text.message}"></c:out></p>

	        <!-- データベースのコメント全件表示 -->

	        <!-- var="comment"は変数名みたいなもんで、
	        	 このjspでは"comment"を使っていくよって言っている。
	        	 "comment"の中にControllerと繋がっているcommentsを入れている。
	        	 なので、下記の29.31行目のcomment.のcommentはcommentの中の、
	        	 messageIdとcommentを取ってきている。 -->

	        <c:forEach items="${comments}" var="comment">
	        	 <!-- ここでmessagesテーブルのidとcommentsテーブルのmessage_id -->
				 <c:if test="${comment.messageId == text.id}">
	        		<!-- 右commentがgetter.setterのと対応 -->
	           	 	<p><c:out value="${comment.comment}"></c:out></p>
	           	 </c:if>
	        </c:forEach>

	        <!-- コメント入力フォーム -->
	        <!-- コメントをデータベースに追加 -->
		    <form:form modelAttribute="commentTextForm">
		    	<!-- setter,getterのcomment -->
		    	<!-- springはpathじゃないと動かん -->
		    	<!-- pathはentityに対応 -->
		        <form:textarea path="comment" cols="20" rows="5"/>
		        <input type="submit" value="コメントしちゃうヨ">
		        <!-- コメントを送るのと同時に、一件取得の時のtextっていう名前の箱に入ってるidを送るのでtext.id
		        	 そのtext.idはmessagesテーブルのid -->
		        <form:hidden path="messageId" value="${text.id}" /><br/>
		    </form:form>
	    </c:forEach>

    </body>
</html>