package jp.co.kenshu.mapper;

import java.util.List;

import jp.co.kenshu.entity.Text;

public interface TextMapper {

	//一件取得
	Text getText(int id);

	//全件取得
	List<Text> getTextAll();

	//データベースに投稿追加
	int insertText(String message);
}
